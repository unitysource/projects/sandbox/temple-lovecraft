﻿using Mehanics.Item;
using UnityEngine;

namespace DataObjects
{
    public enum ItemType
    {
        Plot,
        Technical,
        Personal
    }
    [CreateAssetMenu(fileName = "New Item", menuName = "Data Objects/Inventory/Item")]
    public sealed class Item : ScriptableObject
    {
        public new string name = "New Item";
        public Sprite icon;
        // public bool isDefaultItem;
        public ItemType itemType;

        public void Use()
        {
            // Use the item
            //Something might happen
            Debug.Log($"Using {name}");
        }
    }
}