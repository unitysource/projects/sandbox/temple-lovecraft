﻿using DataObjects;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using Zenject;

namespace Mehanics.Character
{
    public class CharacterStressLight : MonoBehaviour
    {
        private Light2D _globalLight;
        private CharacterProperties _characterProperties;

        private void Start()
        {
            _characterProperties = GetComponent<CharacterPropertiesConfigure>().characterProperties;

            _globalLight = GameObject.Find("Global Light 2D").GetComponent<Light2D>();
        }

        private void FixedUpdate()
        {
            if (_globalLight.intensity < _characterProperties.lightIntensityThreshold)
            {
                Debug.Log("i'm here");
                _characterProperties.characterStress.IncreaseStress(_characterProperties.lightIncreaseSpeed*Time.deltaTime);
            }
        }
    }
}