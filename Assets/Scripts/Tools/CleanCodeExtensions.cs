﻿using System;

namespace Tools
{
    public static class CleanCodeExtensions
    {
        public static T Do<T>(this T obj, Action<T> action) =>
            Do<T>(obj, action);

        public static T Do<T>(this T obj, Action<T> action, bool when)
        {
            if (when)
                action.Invoke(obj);

            return obj;
        }
    }
}