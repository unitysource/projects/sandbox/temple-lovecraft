﻿using System;

namespace Mehanics.Npc_Stuff
{
    public enum CrazyType
    {
        Normal,
        Bad,
        Crazy
    }
    public static class CrazyNpcExtensions
    {
        /// <summary>
        /// We multiple speed of nps and this value
        /// </summary>
        /// <param name="crazyType"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public static float MultiplierForSpeed(CrazyType crazyType)
        {
            switch (crazyType)
            {
                case CrazyType.Normal:
                    return 1f;
                case CrazyType.Bad:
                    return 2f;
                case CrazyType.Crazy:
                    return 3f;
                default:
                    throw new ArgumentOutOfRangeException(nameof(crazyType), crazyType, null);
            }
        }
    }
}