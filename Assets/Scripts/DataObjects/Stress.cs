﻿using UnityEngine;

namespace DataObjects
{
    [CreateAssetMenu(fileName = "Stress File", menuName = "Data Objects/Stress")]
    public class Stress : ScriptableObject
    {
        [SerializeField] private float maxValue;
        [SerializeField] private float minValue;

        public float MaxValue => maxValue;
        public float CurrentValue { get; private set; }

        public void DecreaseStress(float valueToDecrease) => 
            CurrentValue = Mathf.Clamp(CurrentValue - valueToDecrease, minValue, maxValue);

        public void IncreaseStress(float valueToIncrease) => 
            CurrentValue = Mathf.Clamp(CurrentValue + valueToIncrease, minValue, maxValue);

        public float GetNormalizedValue() => CurrentValue / maxValue;
    }
}