﻿using System.Collections.Generic;
using UnityEngine;

namespace Inventory_Stuff
{
    public class InventoryUi : MonoBehaviour
    {
        [SerializeField] private Transform itemsParents;

        private InventorySlot[] _slots;

        private Inventory _inventory;

        private void Start()
        {
            _inventory = Inventory.Instance;
            _inventory.onItemChanged += UpdateUi;
            
            _slots = itemsParents.GetComponentsInChildren<InventorySlot>();
        }

        private void UpdateUi()
        {
            for (int i = 0; i < _slots.Length; i++)
            {
                if (i < _inventory.items.Count)
                {
                    _slots[i].AddItem(_inventory.items[i]);
                }
                else
                {
                    _slots[i].ClearSlot();
                }
            }
        }
    }
}

// namespace Inventory_Stuff
// {
//     public class InventoryUi : MonoBehaviour
//     {
//         [SerializeField] private Transform[] itemsParents;
//
//         private readonly List<InventorySlot[]> _slots = new List<InventorySlot[]>(3); 
//
//         private Inventory _inventory;
//
//         private void Start()
//         {
//             _inventory = Inventory.Instance;
//             _inventory.onItemChanged += UpdateUi;
//
//             foreach (var parent in itemsParents) 
//                 _slots.Add(parent.GetComponentsInChildren<InventorySlot>());
//         }
//
//         private void UpdateUi()
//         {
//             for (int slots = 0; slots < _slots.Count; slots++)
//             {
//                 for (int i = 0; i < _slots[slots].Length; i++)
//                 {
//                     if (slots == 0 && _inventory.items[i].itemType != ItemType.Plot) continue;
//                     if (slots == 1 && _inventory.items[i].itemType != ItemType.Personal) continue;
//                     if (slots == 2 && _inventory.items[i].itemType != ItemType.Technical) continue;
//
//                     if (i < _inventory.items.Count)
//                     {
//                         _slots[slots][i].AddItem(_inventory.items[i]);
//                     }
//                     else
//                     {
//                         _slots[slots][i].ClearSlot();
//                     }
//                 }
//             }
//         }
//     }
// }