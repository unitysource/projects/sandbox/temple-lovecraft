using UnityEngine;
using UnityEngine.Events;

namespace DataObjects.SignalObjects
{
    public class SignalListener : MonoBehaviour
    {
        [SerializeField] private Signal signal;
        [SerializeField] private UnityEvent signalEvent;
    
        public void OnSignalRaise() => signalEvent?.Invoke();

        public void OnEnable() => signal.AddListener(this);

        public void OnDisable() => signal.RemoveListener(this);
    }
}
