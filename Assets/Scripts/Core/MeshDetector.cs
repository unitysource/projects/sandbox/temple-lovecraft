﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Core
{
    public class MeshDetector : MonoBehaviour
    {
        private void Start() => AddPhysicsRaycaster();

        private static void AddPhysicsRaycaster()
        {
            Physics2DRaycaster physicsRaycaster = FindObjectOfType<Physics2DRaycaster>();
            if (physicsRaycaster != null || Camera.main is null) return;
            Camera.main.gameObject.AddComponent<Physics2DRaycaster>();
        }
    }
}