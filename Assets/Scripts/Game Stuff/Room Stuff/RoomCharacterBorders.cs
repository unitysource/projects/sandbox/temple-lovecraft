﻿using UnityEditor;
using UnityEngine;

namespace Game_Stuff
{
    public class RoomCharacterBorders : MonoBehaviour
    {
        [Header("Borders for polygon")] [SerializeField]
        private Vector2 minPosition;

        public Vector2 MinPosition => minPosition;

        [SerializeField] private Vector2 maxPosition;

        public Vector2 MaxPosition => maxPosition;

        private PolygonCollider2D _polygon;

        private void SetPolygonBorders()
        {
            _polygon = GetComponent<PolygonCollider2D>();
            var points = new Vector2[4];

            points[0] = minPosition;
            points[1] = new Vector2(minPosition.x, maxPosition.y);
            points[2] = maxPosition;
            points[3] = new Vector2(maxPosition.x, minPosition.y);
            _polygon.points = points;
        }

        private void SetDefaultBorders()
        {
            var parent = transform.parent;
            RoomCameraBorders parentBorders = parent.GetComponent<RoomCameraBorders>();
            _polygon = GetComponent<PolygonCollider2D>();
            var points = new Vector2[4];
            float maxY = parentBorders.MinPosition.y +
                         (Mathf.Abs(parentBorders.MaxPosition.y) + Mathf.Abs(parentBorders.MinPosition.y)) / 3;

            points[0] = parentBorders.MinPosition;
            points[1] = new Vector2(parentBorders.MinPosition.x, maxY);
            points[2] = new Vector2(parentBorders.MaxPosition.x, maxY);
            points[3] = new Vector2(parentBorders.MaxPosition.x, parentBorders.MinPosition.y);
            minPosition = parentBorders.MinPosition;
            maxPosition = points[2];
            _polygon.points = points;
        }

        public float GetBorderLength(string side = "x")
            => Vector2.Distance(minPosition,
                side is "x"
                    ? new Vector2(maxPosition.x, minPosition.y)
                    : new Vector2(minPosition.x, maxPosition.y)) / 2f;

        public Vector2 GetCenter() =>
            new Vector2((minPosition.x + maxPosition.x) / 2, (minPosition.y + maxPosition.y) / 2);

        public Vector2 GetXCenter() =>
            new Vector2((minPosition.x + maxPosition.x) / 2, minPosition.y);

        public Vector2 GetYCenter(string side = "left") =>
            new Vector2(side is "left" ? minPosition.x : maxPosition.x, (minPosition.y + maxPosition.y) / 2);


#if UNITY_EDITOR
        [CustomEditor(typeof(RoomCharacterBorders))]
        internal class RoomEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                DrawDefaultInspector();
                GUILayout.Space(10);
                var room = (RoomCharacterBorders) target;
                if (GUILayout.Button("Set Default"))
                    room.SetDefaultBorders();
                GUILayout.Space(10);
                if (GUILayout.Button("Set Borders for character movement"))
                    room.SetPolygonBorders();
            }
        }
#endif
    }
}