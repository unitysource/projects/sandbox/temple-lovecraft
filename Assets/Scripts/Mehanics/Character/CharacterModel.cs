﻿using UnityEngine;
using Zenject;

namespace Mehanics.Character
{
    public class CharacterModel : MonoBehaviour
    {
        protected InputControls Input;
        protected Vector2 MoveValue => Input.Player.Move.ReadValue<Vector2>();

        [Inject]
        private void Construct(InputControls input)
        {
            Input = input;
            // Input.Player.Enable();
        }

        // private void OnDisable() => Input.Player.Disable();
    }
}