﻿using System;
using UnityEngine;
using Zenject;

namespace Mehanics.Character
{
    public class CharacterAnimation : CharacterModel
    {
        private Animator _animator;
        private static readonly int Walk = Animator.StringToHash("walk");

        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        private void Update()
        {
            // Debug.LogWarning(MoveValue.x);
            float moveX = MoveValue.x;
            
            if (moveX == 0)
            {
                _animator.SetBool(Walk, false);
            }
            else if (moveX != 0)
            {
                // if(moveX)
                _animator.SetBool(Walk, true);
            }
        }
    }
}