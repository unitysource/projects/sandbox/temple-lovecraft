﻿using UnityEngine;

namespace DataObjects
{
    [CreateAssetMenu(fileName = "New CharacterProperties", menuName = "Data Objects/CharacterProperties")]
    public class CharacterProperties : ScriptableObject
    {
        public Stress characterStress;
        public float lightIntensityThreshold = 0.5f;
        public float searchCorpseStress = 1f;
        public float lightIncreaseSpeed = 1f;
    }
}