﻿using System;
using Mehanics.Character;
using UnityEngine;
using Zenject;

namespace Game_Stuff
{
    public class Input : MonoBehaviour
    {
        private InputControls _input;
        [SerializeField] private Canvas inventory;

        [Inject]
        private void Construct(InputControls input)
        {
            _input = input;
            _input.Enable();

            _input.UI.Inventory.performed += ctx => ChangeInventoryWindowActivity();
        }

        private void Start()
        {
            inventory.enabled = false;
        }

        private void ChangeInventoryWindowActivity()
        {
            inventory.enabled = !inventory.enabled;
        }

        private void OnDisable() => _input.Disable();
    }
}