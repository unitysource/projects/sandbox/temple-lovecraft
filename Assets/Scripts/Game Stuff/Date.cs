﻿using System;
using Core.Data;
using PixelCrushers.DialogueSystem;
using UnityEngine;

namespace Game_Stuff
{
    public class Date : MonoBehaviour
    {
        public void ChangeDate(int value)
        {
            DataController.Day += value;
            Debug.Log(DataController.Day);
        }

        private static bool ConfirmDate(double minDay, double maxDay) =>
            DataController.Day >= minDay && DataController.Day < maxDay;

        #region Lua Configure

        private void OnEnable()
        {
            Lua.RegisterFunction("ConfirmDate", this, SymbolExtensions.GetMethodInfo(() => ConfirmDate(0 , 0)));
        }

        private void OnDisable()
        {
            Lua.UnregisterFunction("ConfirmDate");
        }

        #endregion
    }
}