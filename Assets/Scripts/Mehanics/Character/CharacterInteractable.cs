﻿using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace Mehanics.Character
{
    public class CharacterInteractable : MonoBehaviour
    {
        public bool IsPlayerStay { get; set; }

        [SerializeField] private GameObject context;
        [HideInInspector] public SpriteRenderer contextSprite;
        [HideInInspector] public List<GameObject> interactableObjects;
        
        private void Start()
        {
            contextSprite = context.GetComponent<SpriteRenderer>();
            contextSprite.enabled = false;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer(Layers.Interactable))
            {
                IsPlayerStay = contextSprite.enabled = true;
                interactableObjects.Add(other.gameObject);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer(Layers.Interactable))
            {
                if(IsPlayerStay)
                    IsPlayerStay = contextSprite.enabled = false;
                interactableObjects.Remove(other.gameObject);
            }
        }
    }
}