﻿using System;
using UnityEngine;

namespace Mehanics.Character
{
    public class Context : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer spriteRenderer;

        private void Start()
        {
            spriteRenderer.enabled = false;
        }

        public void EnableChanged()
        {
            spriteRenderer.enabled = !spriteRenderer.enabled;
        }
    }
}