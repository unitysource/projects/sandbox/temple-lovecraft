﻿using System;
using Cinemachine;
using Mehanics.Character;
using UnityEngine;
using Zenject;

namespace Mehanics.Cinemachine
{
    public class CinemachineInitProperties : MonoBehaviour
    {
        private Transform _target;
        
        [Inject]
        private void Construct(CharacterMovement characterMovement)
        {
            _target = characterMovement.gameObject.transform;
            GetComponent<CinemachineVirtualCamera>().Follow = _target;
        }
    }
}