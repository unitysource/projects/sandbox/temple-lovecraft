﻿using Mehanics.Character;
using PixelCrushers.DialogueSystem;
using UnityEngine;
using Zenject;

namespace Game_Stuff
{
    public class AddPlayerForDialogs : MonoBehaviour
    {
        // [SerializeField] private DialogueSystemTrigger dialogueSystemTrigger;
        [Inject]
        private void Construct(CharacterMovement movement)
        {
            // if (GetComponent<DialogueSystemTrigger>().conversationActor != null)
                GetComponent<DialogueSystemTrigger>().conversationActor = movement.transform;
            // Debug.Log(GetComponent<DialogueSystemTrigger>().barker);
            // if (GetComponent<DialogueSystemTrigger>().barker != null)
                GetComponent<DialogueSystemTrigger>().barker = movement.transform;
        }
    }
}