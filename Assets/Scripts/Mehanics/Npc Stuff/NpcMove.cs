﻿using System.Collections;
using Core.DataObjects;
using UnityEngine;

namespace Mehanics.Npc_Stuff
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class NpcMove : MonoBehaviour
    {
        [HideInInspector] public Rigidbody2D rb;
        [SerializeField] private Direction currentDirection = Direction.Left;
        [SerializeField] private float startSpeed = 3f;
        [SerializeField] private ValuesRange changeDirectionDelay;

        [SerializeField] private CrazyType currentCrazyType;

        private void Start()
        {
            rb = GetComponent<Rigidbody2D>();

            Move(currentDirection, startSpeed, currentCrazyType);

            StartCoroutine(ChangeDirectionIe());
        }

        private void Move(Direction dir, float speed, CrazyType crazyType)
        {
            int sign = DirectionExtensions.Sign(dir);
            float newSpeed = speed * CrazyNpcExtensions.MultiplierForSpeed(crazyType);
            rb.velocity = Vector2.right * (sign * newSpeed);
            currentDirection.RandomChangeDirection();
            Debug.Log(newSpeed);
        }

        private IEnumerator ChangeDirectionIe()
        {
            while (gameObject.activeSelf)
            {
                yield return new WaitForSeconds(changeDirectionDelay.GetRandom());
                // Debug.Log($"{currentDirection} speed: {currentSpeed}");
                Move(currentDirection, startSpeed, currentCrazyType);
            }
        }
    }
}