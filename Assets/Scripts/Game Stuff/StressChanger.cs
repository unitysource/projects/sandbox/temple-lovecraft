﻿using System;
using DataObjects;
using PixelCrushers.DialogueSystem;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

namespace Game_Stuff
{
    public class StressChanger : MonoBehaviour
    {
        [SerializeField] private Image stressFilled;
        [SerializeField] private AudioSource stressSource;
        
        [SerializeField] private PostProcessVolume postProcessVolume;
        [SerializeField] private Stress stress;
        [SerializeField] private float changeStressDelay = 0.5f;

        private Vignette _stressVignette;
        private Stress _characterStress;

        private void Start()
        {
            postProcessVolume.enabled = true; 
            _stressVignette = postProcessVolume.profile.GetSetting<Vignette>();
            
            // stressSource.volume = 0;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tolerance">between 0 and 10</param>
        /// <returns></returns>
        private bool CanViewAllConditions(double tolerance)
        {
            return stress.CurrentValue < tolerance;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value">from -5f to 5f</param>
        private void ChangeStress(double value)
        {
            stress.IncreaseStress((float) value);
        }

        private void FixedUpdate()
        {
            _stressVignette.intensity.value = stressFilled.fillAmount = stressSource.volume =
                Mathf.Lerp(_stressVignette.intensity.value, stress.GetNormalizedValue(), changeStressDelay*Time.deltaTime);
        }

        #region Lua Configure

        private void OnEnable()
        {
            Lua.RegisterFunction("CanViewAllConditions", this, SymbolExtensions.GetMethodInfo(()=>CanViewAllConditions(0)));
            Lua.RegisterFunction("ChangeStress", this, SymbolExtensions.GetMethodInfo(()=>ChangeStress(0)));
        }

        private void OnDisable()
        {
            Lua.UnregisterFunction("CanViewAllConditions");
            Lua.UnregisterFunction("ChangeStress");
        }
        
        #endregion
    }
}