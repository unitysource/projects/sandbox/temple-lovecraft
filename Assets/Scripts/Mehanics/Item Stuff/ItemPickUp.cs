﻿using Inventory_Stuff;
using UnityEngine;

namespace Mehanics.Item
{
    public class ItemPickUp : MonoBehaviour
    {
        [SerializeField] private DataObjects.Item item;
        public void PickUp()
        {
            Debug.Log($"Picking up [{item.name}]");
            bool wasPickedUp = Inventory.Instance.Add(item);
            if(wasPickedUp)
                Destroy(gameObject);
        }
    }
}