﻿﻿using UnityEngine;
 using UnityEngine.UI;

 namespace Core
{
    namespace Data
    {
        public static class DataController
        {
            private const string DataDay = "day";
            // private const string DataHighscore = "highscore";

            private const string DataIsMusicOn = "music";
            private const string DataIsSoundsOn = "sounds";
            private const string DataHardcoreMode = "hardcore";
            private const string DataLanguage = "language";
            private const string DataDeathCounter = "deathCounter";
            private const int DefaultInt = 0;

            public const int MaxLevel = 6;
            // public static string Language
            // {
            //     get => GetString(DataLanguage);
            //     set => SaveString(DataLanguage, value);
            // }

            // How many stars is Player collected
            public static int StarsCount { get; set; }
            public static float LevelTime { get; set; }
            
            public static float Health { get; set; }
            
            //Player death count in hardcore mode
            public static int DeathCounter
            {
                get => GetInt(DataDeathCounter);
                set => SaveInt(DataDeathCounter, value);
            }


            #region Properties

            public static bool IsMusicOn
            {
                get => GetInt(DataIsMusicOn) == 1;
                set => SaveInt(DataIsMusicOn, value ? 1 : 0);
            }

            public static bool IsSoundsOn
            {
                get => GetInt(DataIsSoundsOn) == 1;
                set => SaveInt(DataIsSoundsOn, value ? 1 : 0);
            }

            /// <summary>
            /// If its first game - reset all values
            /// </summary>
            public static void FirstGame()
            {
                var hasPlayed = GetInt("HasPlayed");
                // if it's NOT first game
                if (hasPlayed != 0)
                {
                }
                // if it's first game
                else
                {
                    IsMusicOn = IsSoundsOn = true;
                    SaveInt("HasPlayed", 1);
                    SaveInt(DataHardcoreMode, 0);
                    
                    // var startLanguage = "English";
                    var systemLanguage = Application.systemLanguage.ToString();
                    // foreach (var language in Localization.Languages)
                    //     if (language == systemLanguage)
                    //         startLanguage = systemLanguage;

                    // Language = startLanguage;
                    // Debug.Log(startLanguage);
                }
            }

            public static int Day
            {
                get => GetInt(DataDay);
                set
                {
                    // soft clamp value at 0
                    if (value < 0) value = 0;
                    SaveInt(DataDay, value);
                }
            }

            #endregion


            #region Private Functions

            private static void SaveInt(string data, int value) =>
                PlayerPrefs.SetInt(data, value);

            private static int GetInt(string data) =>
                PlayerPrefs.GetInt(data, DefaultInt);
            
            private static void SaveFloat(string data, float value) =>
                PlayerPrefs.SetFloat(data, value);

            private static float GetFloat(string data) =>
                PlayerPrefs.GetFloat(data, DefaultInt);
            
            private static void SaveString(string data, string value) =>
                PlayerPrefs.SetString(data, value);

            private static string GetString(string data) =>
                PlayerPrefs.GetString(data);

            #endregion
        }
    }
}