﻿using Mehanics.Character;
using Tools;
using UnityEngine;
using Zenject;

namespace Infrastructures
{
    public class LocationInstaller : MonoInstaller
        // , IInitializable
    {
        [SerializeField] private Transform startPoint;

        [SerializeField] private GameObject playerPrefab;

        // private GameObject _character;

        // [SerializeField] private EnemyMarker[] enemyMarkers;

        public override void InstallBindings()
        {
            BindInstallerInterfaces();
            BindInput();
            BindCharacter();
            // BindCharacterProperties();

            // BindEnemyFactory();
        }

        // private void BindCharacterProperties()
        // {
        //     Container.Bind<CharacterProperties>()
        //         .FromInstance(_character.GetComponent<CharacterPropertiesConfigure>().characterProperties).AsSingle();
        // }
        
        private void BindInstallerInterfaces()
        {
            Container.BindInterfacesTo<LocationInstaller>().FromInstance(this).AsSingle();
        }

        private void BindInput()
        {
            var input = new InputControls();
            Container.Bind<InputControls>().FromInstance(input.With(_ => input.Enable())).AsSingle();
        }

        private void BindEnemyFactory()
        {
            // Container.Bind<IEnemyFactory>().To<EnemyFactory>().AsSingle();
        }

        private void BindCharacter()
        {
            var characterMovement = Container.InstantiatePrefabForComponent<CharacterMovement>(playerPrefab,
                startPoint.position, Quaternion.identity, null);
            Container.Bind<CharacterMovement>().FromInstance(characterMovement).AsSingle();
            
            // Debug.Log(characterModel.GetComponent<CharacterPropertiesConfigure>());
            // Container.Bind<CharacterProperties>()
            //     .FromInstance(characterModel.GetComponent<CharacterPropertiesConfigure>().characterProperties).AsSingle();
        }
    }
}