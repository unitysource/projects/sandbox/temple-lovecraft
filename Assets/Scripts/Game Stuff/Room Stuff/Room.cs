﻿using System.Collections;
using Cinemachine;
using Core;
using UnityEngine;

namespace Game_Stuff.Room_Stuff
{
    /// <summary>
    /// I should call folders in RoomObject correctly (Enemies, Pots)
    /// </summary>
    [RequireComponent(typeof(PolygonCollider2D))]
    public class Room : MonoBehaviour
    {
        [Header("Delay before doors will closing when player enter to enemy room")] [SerializeField]
        protected float playerEnterDelay = .3f;

        // protected List<EnemyModel> Enemies;
        // private List<PotCrash> _pots;
        private GameObject _virtualCamera;

        protected virtual void Start()
        {
            _virtualCamera = transform.GetComponentInChildren<CinemachineVirtualCamera>().gameObject;
            _virtualCamera.SetActive(false);

            // Enemies = new List<EnemyModel>();
            // _pots = new List<PotCrash>();

            // I have two types of enemies. 1-EnemyModel component in parent and without children. 2-EnemyModel component in child
            // foreach (Transform enemyObj in transform.Find("Enemies"))
            //     Enemies.Add(enemyObj.TryGetComponent<EnemyModel>(out var enemyModel)
            //         ? enemyModel
            //         : enemyObj.GetComponentInChildren<EnemyModel>());

            // foreach (Transform potObj in transform.Find("Pots"))
            //     _pots.Add(potObj.GetComponent<PotCrash>());

            //ToDo Maybe it's not good
            // foreach (var enemy in Enemies) ActiveChange(enemy, false);
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag(Tags.Player) || other.isTrigger) return;
            StartCoroutine(PlayerEnterDelayIe());
        }

        protected void OnTriggerExit2D(Collider2D other)
        {
            if (!other.CompareTag(Tags.Player) || other.isTrigger) return;
            
            //Deactivate all enemies and pots
            // foreach (var enemy in Enemies) ActiveChange(enemy, false);
            // foreach (var pot in _pots) ActiveChange(pot, false);

            _virtualCamera.SetActive(false);
        }

        protected virtual IEnumerator PlayerEnterDelayIe()
        {
            yield return new WaitForSeconds(playerEnterDelay);
            
            //Activate all enemies and pots
            // foreach (var enemy in Enemies) ActiveChange(enemy, true);
            // foreach (var pot in _pots) ActiveChange(pot, true);

            // if(virtualCamera != null) 
            _virtualCamera.SetActive(true);
        }

        private static void ActiveChange(Component component, bool isActivate) =>
            component.gameObject.SetActive(isActivate);
    }
}