﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataObjects;
using UnityEngine;

namespace Inventory_Stuff
{
    public class Inventory : MonoBehaviour
    {
        #region Singleton
        
        public static Inventory Instance;
        
        private void Awake()
        {
            if (Instance != null)
            {
                Debug.LogWarning("More than one instance of Inventory found");
                return;
            }
            Instance = this;
        }

        #endregion

        public Action onItemChanged;

        [SerializeField] private int space = 20;
        public List<Item> items = new List<Item>();

        public bool Add(Item item)
        {
            // if (item.isDefaultItem) return true;
            
            if (items.Count >= space)
            {
                Debug.Log("Not enough place in inventory!");
                return false;
            }

            items.Add(item);
            //Adding Item to Slot
            onItemChanged?.Invoke();
            return true;
        }
        
        public void Remove(Item item)
        {
            items.Remove(item);
            //Clear Slot
            onItemChanged?.Invoke();
        }
    }
}