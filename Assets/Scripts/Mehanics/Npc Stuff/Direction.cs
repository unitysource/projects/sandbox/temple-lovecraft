﻿using UnityEngine;

namespace Mehanics.Npc_Stuff
{
    public enum Direction
    {
        Left,
        Right,
        Idle
    }

    public static class DirectionExtensions
    {
        public static int Sign(Direction dir) => 
            dir is Direction.Left ? -1 : dir is Direction.Right ? 1 : 0;

        public static void RandomChangeDirection(this ref Direction current)
        {
            int counter = 0;
            int rand = Random.Range(0, 3);
            Direction newDir = current;
            while (newDir == current && counter < 100)
            {
                newDir = rand == 0 ? Direction.Left : rand == 1 ? Direction.Idle : Direction.Right;
                counter++;
            }

            current = newDir;
        }
    }
}