﻿using System.Linq;
using Core;
using DataObjects;
using Mehanics.Item;
using UnityEngine;

namespace Mehanics.Character
{
    public class CharacterInputInteract : CharacterModel
    {
        private CharacterInteractable _characterInteractable;
        private CharacterProperties _characterProperties;

        private void Start()
        {
            _characterProperties = GetComponent<CharacterPropertiesConfigure>().characterProperties;
            _characterInteractable = GetComponent<CharacterInteractable>();
            Input.Player.Interact.performed += ctx =>
            {
                if (!_characterInteractable.IsPlayerStay) return;
                Debug.Log("Interact Act");
                SearchCorpse();

                PickUpFirst();
            };
        }

        private void PickUpFirst()
        {
            foreach (var interactableObject in _characterInteractable.interactableObjects)
            {
                if(interactableObject.TryGetComponent<ItemPickUp>(out var itemPickUp))
                {
                    itemPickUp.PickUp();
                    return;
                }
            }
        }

        private void SearchCorpse()
        {
            foreach (var item in _characterInteractable.interactableObjects.Where(item => item.CompareTag(Tags.Corpse)))
            {
                Debug.Log("I'm searching corpse!");

                _characterProperties.characterStress.IncreaseStress(_characterProperties.searchCorpseStress);
                _characterInteractable.IsPlayerStay = _characterInteractable.contextSprite.enabled = false;
                item.layer = default;
                break;
            }
        }
    }
}