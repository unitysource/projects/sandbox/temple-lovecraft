﻿﻿#if UNITY_EDITOR

 using System;
 using System.IO;
 using JetBrains.Annotations;
 using UnityEditor;
 using UnityEngine;

 namespace Core
{
    [CustomPropertyDrawer(typeof(DrawIfAttribute))]
    public class DrawIfAttributeDrawer : PropertyDrawer
    {
        #region Fields

        // Reference to the attribute on the property.
        private DrawIfAttribute _drawIf;

        // Field that is being compared.
        private SerializedProperty _comparedField;

        #endregion

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return !TryShowMe(property) ? 0f : base.GetPropertyHeight(property, label);

            // The height of the property should be defaulted to the default height.
        }

        /// <summary>
        /// Errors default to showing the property.
        /// </summary>
        private bool TryShowMe(SerializedProperty property)
        {
            bool result = ShowMe(property, out string errorMessage);
            if (errorMessage != null) Debug.LogError(errorMessage);
            return result;
        }


        private bool ShowMe(SerializedProperty property, [CanBeNull] out string errorMessage)
        {
            errorMessage = null;
            _drawIf = attribute as DrawIfAttribute;
            // Replace property name to the value from the parameter
            if (_drawIf is null) return true;

            string path = property.propertyPath.Contains(".")
                ? Path.ChangeExtension(property.propertyPath, _drawIf.PropName)
                : _drawIf.PropName;

            _comparedField = property.serializedObject.FindProperty(path);

            // ReSharper disable once InvertIf
            if (_comparedField is null)
            {
                errorMessage = $"Cannot find property with name: {path}";
                return true;
            }

            // get the value & compare based on types
            switch (_comparedField.type)
            {
                case "bool":
                    return _comparedField.boolValue.Equals(_drawIf.Value);
                case "Enum":
                    return _comparedField.enumValueIndex.Equals(Convert.ToInt32(_drawIf.Value));
                default:
                    errorMessage = $"Error: {_comparedField.type} is not supported of {path}";
                    return true;
            }
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // If the condition is met, simply draw the field.
            if (TryShowMe(property)) EditorGUI.PropertyField(position, property);
        }
    }
}

#endif