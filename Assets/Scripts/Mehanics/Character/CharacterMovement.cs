﻿using System;
using UnityEngine;
using Zenject;

namespace Mehanics.Character
{
    public class CharacterMovement : CharacterModel
    {
        [SerializeField] private float speed;
        [HideInInspector] public Rigidbody2D rb;
        private PolygonCollider2D _moveBorders;

        private float _startSpeed;
        private float _startXScale;
        
        [Tooltip("if 1 - Character isn't flipped")]
        private float _currentSign = 1;

        private void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            _startSpeed = speed;
            _startXScale = transform.localScale.x;
            // _moveBorders = GameObject.Find("Room Character Borders").GetComponent<PolygonCollider2D>();
        }

        private void Update()
        {
            Vector2 m = new Vector2(MoveValue.x * speed, 0);
            var myTransform = transform;
            var localScale = myTransform.localScale;
            
            float sign = _currentSign;
            if (m.x != 0) _currentSign = m.x < 0 ? -1 : 1;

            localScale = new Vector3(_startXScale * sign, localScale.y, localScale.z);

            myTransform.localScale = localScale;
            rb.velocity = m;
        }

        public void StopMove() => 
            speed = 0;

        public void StartMove() => 
            speed = _startSpeed;
    }
}